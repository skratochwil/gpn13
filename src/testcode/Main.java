package testcode;

import model.Connection;
import model.DecisionEngine;
import model.debugger.PrintfDebugger;

public class Main {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PrintfDebugger debugger = new PrintfDebugger();
		
		Connection c = new Connection("spacegoo.gpn.entropia.de", 6000, debugger);
		DecisionEngine engine = new DecisionEngine(debugger);
		
		c.open();
		c.login("hoedur", "1qay2wsx3edc");
		
		String serverReply;
		String myDecision;
		for (int i = 0; ; i++) {
			serverReply = c.read();
			debugger.write(serverReply);
			
			if (serverReply.startsWith("{")) {
				engine.acceptString(serverReply);

				myDecision = engine.getDecision();
				debugger.write(myDecision);
				
				c.write(myDecision);
			} 
		}
	}

}
