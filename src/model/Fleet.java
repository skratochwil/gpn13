package model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/*
 * "fleets": [
    {
      "id": 0,                  
      "owner_id": 1,            // player_id
      "origin": 1,              // planet_id
      "target": 2,              // plane_id
      "ships": [ 3, 1, 1 ],     // Anzahlen Typ a, b, c
      "eta": 45                 // Ankunftsrunde (s.u.)
    },
 */
public class Fleet {
	private int id;
	private int ownerID;
	private int origin;
	private int target;
	private int nShipsA;
	private int nShipsB;
	private int nShipsC;
	private int eta;
	
	public Fleet(JSONObject fleetObject) {
		this.id = fleetObject.getInt("id");
		this.ownerID = fleetObject.getInt("owner_id");
		this.origin = fleetObject.getInt("origin");
		this.target = fleetObject.getInt("target");
		
		try {
			JSONArray shipArray = fleetObject.getJSONArray("ships");
			this.nShipsA = shipArray.getInt(0);
			this.nShipsB = shipArray.getInt(1);
			this.nShipsC = shipArray.getInt(3);
		} catch (JSONException e) {
			this.nShipsA = 0;
			this.nShipsB = 0;
			this.nShipsC = 0;
		}
		
		this.eta = fleetObject.getInt("eta");
	}
	
	public Fleet(int id, int ownerID, int origin, int target, int nShipsA,
			int nShipsB, int nShipsC, int eta) {
		super();
		this.id = id;
		this.ownerID = ownerID;
		this.origin = origin;
		this.target = target;
		this.nShipsA = nShipsA;
		this.nShipsB = nShipsB;
		this.nShipsC = nShipsC;
		this.eta = eta;
	}
	public int getId() {
		return id;
	}
	public int getOwnerID() {
		return ownerID;
	}
	public int getOrigin() {
		return origin;
	}
	public int getTarget() {
		return target;
	}
	public int getnShipsA() {
		return nShipsA;
	}
	public int getnShipsB() {
		return nShipsB;
	}
	public int getnShipsC() {
		return nShipsC;
	}
	public int getEta() {
		return eta;
	}
	
	
}
