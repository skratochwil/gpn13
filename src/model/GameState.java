package model;

import java.util.Comparator;
import java.util.LinkedList;

import model.PlanetComparatorFactory.SortStrategy;

/*
 * {
  "game_over": false,
  "winner": null,               // player_id
  "round": 2
  "max_rounds": 500,
  "fleets": [
    {
      "id": 0,                  
      "owner_id": 1,            // player_id
      "origin": 1,              // planet_id
      "target": 2,              // plane_id
      "ships": [ 3, 1, 1 ],     // Anzahlen Typ a, b, c
      "eta": 45                 // Ankunftsrunde (s.u.)
    },
    ...
  ],
  "players": [
    {
      "id": 1,
      "name": "dividuum",
      "itsme": true
    },
    {
      "id": 2,
      "name": "cupe",
      "itsme": false
    }
  ],
  "planets": [
    {
      "id": 0,
      "owner_id": 0,            // player_id
      "y": 0,                   
      "x": 0,
      "ships": [ 20, 20, 20 ],  // aktuelle Schiffsanzahl Typ a, b, c
      "production": [ 1, 1, 1 ] // Produktion/Runde Typ a, b, c
    },
    ...
  ],
}
 */

public class GameState {
	private boolean gameOver;
	private int playerID;
	private int winnerID;
	private int round;
	private int maxRounds;
	private LinkedList<Fleet> fleets;
	private LinkedList<Player> players;
	private LinkedList<Planet> planets;
	
	static enum PlanetSortStrategy {
		descByTotalShipCount,
		descByShipCountA,
		descByShipCountB,
		descByShipCountC
	}
	
	public GameState(boolean gameOver, 
			int winnerID, 
			int playerID, 
			int round, 
			int maxRounds,
			LinkedList<Fleet> fleets, 
			LinkedList<Player> players,
			LinkedList<Planet> planets) {
		super();
		this.gameOver = gameOver;
		this.winnerID = winnerID;
		this.playerID = playerID;
		this.round = round;
		this.maxRounds = maxRounds;
		this.fleets = fleets;
		this.players = players;
		this.planets = planets;
	}
	public boolean isGameOver() {
		return gameOver;
	}
	public int getPlayerID() {
		return playerID;
	}
	public int getWinner() {
		return winnerID;
	}
	public int getRound() {
		return round;
	}
	public int getMaxRounds() {
		return maxRounds;
	}
	public LinkedList<Fleet> getFleets() {
		return fleets;
	}
	public LinkedList<Player> getPlayers() {
		return players;
	}
	public LinkedList<Planet> getPlanets() {
		return planets;
	}
	
	public int getMyPlayerID() {
		for (int i = 0; i < this.players.size(); i++) {
			if (this.players.get(i).isItsme() == true) {
				return this.players.get(i).getId();
			}
		}
		
		throw new SpacegooException("Could not determine my player ID");
	}
	
	public Planet get(int id) {
		for (int i = 0; i < planets.size(); i++) {
			if (planets.get(i).getId() == id)
				return planets.get(1);
		}
		
		throw new SpacegooException("Invalid planet id");
	}
	
	public LinkedList<Planet> getMyPlanets() {
		LinkedList<Planet> myPlanets = new LinkedList<Planet>();
		
		for (int i = 0; i < this.planets.size(); i++) {
			if (this.planets.get(i).getOwnderId() == this.getMyPlayerID()) {
				myPlanets.add(this.planets.get(i));
			}
		}
		
		return myPlanets;
	}
	
	private LinkedList<Planet> getMyPlanetsSorted(Comparator<Planet> comp) {
		LinkedList<Planet> myPlanets = getMyPlanets();
		java.util.Collections.sort(myPlanets, comp);
		return myPlanets;
	}
	
	public LinkedList<Planet> getMyPlanetsSorted(SortStrategy strategy) {
		return getMyPlanetsSorted(PlanetComparatorFactory.get(strategy));
	}
	
	public LinkedList<Planet> getEnemyPlanets() {
		LinkedList<Planet> enemyPlanets = new LinkedList<Planet>();
		
		for (int i = 0; i < this.planets.size(); i++) {
			int planetOwnerID = this.planets.get(i).getOwnderId();
			int myID = this.getMyPlayerID();
			
			if (planetOwnerID != myID) {
				Planet p = this.planets.get(i);
				enemyPlanets.add(p);
			}
		}
		
		return enemyPlanets;
	}
}
