package model;

import org.json.JSONObject;

/*
 *       "id": 1,
      "name": "dividuum",
      "itsme": true
 */
public class Player {
	private int id;
	private String name;
	private boolean itsme;
	public Player(int id, String name, boolean itsme) {
		super();
		this.id = id;
		this.name = name;
		this.itsme = itsme;
	}
	public Player(JSONObject jsonObject) {
		this.id = jsonObject.getInt("id");
		this.name = jsonObject.getString("name");
		this.itsme = jsonObject.getBoolean("itsme");
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public boolean isItsme() {
		return itsme;
	}
	
	
}
