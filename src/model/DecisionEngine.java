package model;

import java.util.LinkedList;
import java.util.List;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

import model.PlanetComparatorFactory.SortStrategy;
import model.debugger.IDebugger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DecisionEngine {
	private IDebugger dbg;
	private String rawJsonString;
	private GameState currentState;
	
	public DecisionEngine(IDebugger debugger) {
		if (debugger != null) {
			this.dbg = debugger;
		} else {
			this.dbg = new IDebugger() {
				@Override
				public void write(String msg) {
					// do nothing
				}
			};
		}
	}
	
	private LinkedList<Fleet> getFleets(JSONObject o) {
		JSONArray jsonFleets = o.getJSONArray("fleets");
		LinkedList<Fleet> fleets = new LinkedList<Fleet>();
		
		for (int i = 0; i < jsonFleets.length(); i++) {
			fleets.add(new Fleet(jsonFleets.getJSONObject(i)));
		}
		
		return fleets;
	}
	
	private LinkedList<Player> getPlayers(JSONObject o) {
		JSONArray jsonPlayers = o.getJSONArray("players");
		LinkedList<Player> players = new LinkedList<Player>();
		
		for (int i = 0; i < jsonPlayers.length(); i++) {
			players.add(new Player(jsonPlayers.getJSONObject(i)));
		}
		
		return players;
	}
	
	private LinkedList<Planet> getPlanets(JSONObject o) {
		JSONArray jsonPlanets = o.getJSONArray("planets");
		LinkedList<Planet> planets = new LinkedList<Planet>();
		
		for (int i = 0; i < jsonPlanets.length(); i++) {
			planets.add(new Planet(jsonPlanets.getJSONObject(i)));
		}
		
		return planets;
	}
	
	private void processRawData() {
		JSONObject o = new JSONObject(this.rawJsonString);
		
		boolean gameOver = o.getBoolean("game_over");
		
		int winnerId;
		try {
			winnerId = o.getInt("winner");
		} catch (JSONException e) {
			dbg.write("winner is 'null'");
			winnerId = 0;
		}
		
		int playerId = o.getInt("player_id");
		int currentRound = o.getInt("round");
		int maxRounds = o.getInt("max_rounds");
		LinkedList<Fleet> fleets = getFleets(o);
		LinkedList<Player> players = getPlayers(o);
		LinkedList<Planet> planets = getPlanets(o);
		
		currentState = new GameState(gameOver, winnerId, playerId, currentRound,
				maxRounds, fleets, players, planets);
		
//		dbg.write(this.rawJsonString);
		dbg.write("State calculated.");
	}


	
	private int getBestSourcePlanet() {
		List<Planet> myPlanets = currentState.getMyPlanetsSorted(SortStrategy.descendingNumShipsTotal);
		return myPlanets.get(0).getId();
	}
	
	private int getBestSourcePlanet(int targetPlanet) {
		List<Planet> myPlanets = currentState.getMyPlanetsSorted(SortStrategy.descendingNumShipsTotal);
		
		Planet target = currentState.get(targetPlanet);
		
		int minDistance = Integer.MAX_VALUE;
		int currentDistance;
		int bestSourcePlanetID = 0;
		
		for (int i = 0; i < myPlanets.size(); i++) {
			currentDistance = SpacegooMath.getDistance(target, myPlanets.get(i));
			if (currentDistance < minDistance) {
				minDistance = currentDistance;
				bestSourcePlanetID = myPlanets.get(i).getId();
			}
		}

		return bestSourcePlanetID;
	}
	
	private int getBestTargetPlanet() {
		List<Planet> enemyPlanets = currentState.getEnemyPlanets();
		return enemyPlanets.get(0).getId();
	}
	
	public void acceptString(String input) {
		this.rawJsonString = input;
		processRawData();
	}
	
	public String getDecision() {
		int bestTargetPlanet = getBestTargetPlanet();
		int bestSourcePlanet = getBestSourcePlanet(bestTargetPlanet);
		String decision = "send " + String.valueOf(bestSourcePlanet) + " " + String.valueOf(bestTargetPlanet) + " 9999 9999 9999";
		return decision;
	}
}
