package model;

import org.json.JSONArray;
import org.json.JSONObject;

/*
 *  "planets": [
    {
      "id": 0,
      "owner_id": 0,            // player_id
      "y": 0,                   
      "x": 0,
      "ships": [ 20, 20, 20 ],  // aktuelle Schiffsanzahl Typ a, b, c
      "production": [ 1, 1, 1 ] // Produktion/Runde Typ a, b, c
    },
 */
public class Planet {
	private int id;
	private int ownderId;
	private int x;
	private int y;
	private int numShipsA;
	private int numShipsB;
	private int numShipsC;
	private int rateShipsA;
	private int rateShipsB;
	private int rateShipsC;
	public Planet(int id, int ownderId, int x, int y, int numShipsA,
			int numShipsB, int numShipsC, int rateShipsA, int rateShipsB,
			int rateShipsC) {
		super();
		this.id = id;
		this.ownderId = ownderId;
		this.x = x;
		this.y = y;
		this.numShipsA = numShipsA;
		this.numShipsB = numShipsB;
		this.numShipsC = numShipsC;
		this.rateShipsA = rateShipsA;
		this.rateShipsB = rateShipsB;
		this.rateShipsC = rateShipsC;
	}
	public Planet(JSONObject jsonObject) {
		this.id = jsonObject.getInt("id");
		this.ownderId = jsonObject.getInt("owner_id");
		this.x = jsonObject.getInt("x");
		this.y = jsonObject.getInt("y");
		
		JSONArray numShips = jsonObject.getJSONArray("ships");
		this.numShipsA = numShips.getInt(0);
		this.numShipsB = numShips.getInt(1);
		this.numShipsC = numShips.getInt(2);
		
		JSONArray rateShips = jsonObject.getJSONArray("production");
		this.rateShipsA = rateShips.getInt(0);
		this.rateShipsB = rateShips.getInt(1);
		this.rateShipsC = rateShips.getInt(2);
	}
	public int getId() {
		return id;
	}
	public int getOwnderId() {
		return ownderId;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public int getNumShipsA() {
		return numShipsA;
	}
	public int getNumShipsB() {
		return numShipsB;
	}
	public int getNumShipsC() {
		return numShipsC;
	}
	public int getNumShipsTotal() {
		return numShipsA + numShipsB + numShipsC;
	}
	public int getRateShipsA() {
		return rateShipsA;
	}
	public int getRateShipsB() {
		return rateShipsB;
	}
	public int getRateShipsC() {
		return rateShipsC;
	}
	
	@Override
	public String toString() {
		String s = 	"Planet ID: " + String.valueOf(this.id) + "\n" +
					"\t Owner: " + String.valueOf(this.ownderId) + "\n" +
					"\t numShipsA: " + String.valueOf(this.numShipsA) + "\t rateShipsA: " + String.valueOf(this.rateShipsA) + "\n" +
					"\t numShipsB: " + String.valueOf(this.numShipsB) + "\t rateShipsA: " + String.valueOf(this.rateShipsA) + "\n" +
					"\t numShipsC: " + String.valueOf(this.numShipsC) + "\t rateShipsA: " + String.valueOf(this.rateShipsA) + "\n";
		return s;
	}
	
	
	
}
