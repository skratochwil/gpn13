package model;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import model.debugger.IDebugger;

public class Connection {
	private IDebugger dbg;
	private Socket sock;
	private String hostname;
	private int port;
	
	private BufferedReader reader;
	private PrintWriter writer;
	
	public Connection(String host, int port, IDebugger debugger) {
		this.hostname = host;
		this.port = port;
		
		if (debugger != null) {
			dbg = debugger;
		} else {
			dbg = new IDebugger() {
				@Override
				public void write(String msg) {
					// does nothing 
				}
			};
		}

	}
	
	public void open() {
		try {
			sock = new Socket(this.hostname, this.port);
			reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			writer = new PrintWriter(sock.getOutputStream(), true);
		} catch (UnknownHostException e) {
			dbg.write(e.getMessage());
			System.exit(1);
		} catch (IOException e) {
			dbg.write(e.getMessage());
			System.exit(1);
		}
		
	}
	
	public String read() {
		try {
			return reader.readLine();
		} catch (IOException e) {
			dbg.write(e.getMessage());
			System.exit(1);
			
			// to please the compiler...
			return "";
		}
	}
	
	public void login(String user, String pwd) {
		String loginString = "login" + " " + user + " " + pwd;
		dbg.write(loginString);
		writer.println(loginString);
		//writer.flush();
	}
	
	public void write(String data) {
		writer.println(data);
	}
}
